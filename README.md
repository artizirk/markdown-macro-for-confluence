Confluence Markdown Macro
========================

This macro uses the Flexmark library to convert from Markdown to HTML within Confluence.

It can be accessed via:

*   Macro Browser
*   {markdown} tags
*   SOAP API using <ac:macro ac:name="markdown"></ac:macro>

This macro supports the following languages:

*   English
*   French
*   German
